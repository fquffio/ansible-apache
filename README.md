Apache
======

This role's duty is to set up [Apache HTTP server](https://httpd.apache.org/), virtual hosts, and SSL certificates issued via [Letsencrypt](https://letsencrypt.org/).

Variables
---------

### `apache_servername`
The main `ServerName`. By default, this is the full machine hostname.

### `apache_serveradmin`
The email address used for technical issues. By default, this is `webmaster@{{ ansible_fqdn }}`.

### `apache_modules`
A list of Apache modules to be enabled, **not** prefixed by `mod_`. By default, only `mod_alias`, `mod_rewrite`, `mod_ssl` and `mod_vhost_alias` are enabled.

### `apache_homepage`
The contents of the file `/var/www/html/index.html`. If left empty, the existing homepage will not be replaced.

### `apache_vhosts`
A list of virtual hosts to be created. Each virtual host is a dictionary with the following keys:

- a **required** key `key`, to uniquely idenfify the virtual host
- an *optional* key `webroot`, the `DocumentRoot` for the current virtual host (default: `/var/www/html`)
- an *optional* key `vdocroot`, the `VirtualDocumentRoot` for the current virtual host (if defined, `webroot` is discarded)
- an *optional* key `domain`, the `ServerName` for the current virtual host
- an *optional* key `aliases`, a list of `ServerAlias`es for the current virtual host
- an *optional* key `email`, the `ServerAdmin` for the current virtual host (default: same as `apache_serveradmin`)
- an *optional* key `letsencrypt`, a boolean flag to set whether a SSL certificate for the current virtual host should be issued via Letsencrypt (default: `false`)

### `php_extensions`
A list of PHP extensions to be installed via APM, **prefixed** with `php5-`. By default, `curl`, `gd`, `intl`, `mcrypt`, `mysql`, `redis` and `xcache` are installed.

### `letsencrypt_path`
The path where Letsencrypt command line tool should be stored. By default, this is `/opt/letsencrypt`.

### `letsencrypt_staging`
A boolean flag to set whether certificates should be issued and renewed via the test server — certificates issued with this option turned on will not be signed by any trusted CA, so this should be used only for testing purposes. By default, this option is turned off.

Example
-------

```yaml
---
apache_servername: example.com
apache_serveradmin: webmaster@example.com
apache_modules:
  - alias
  - proxy
  - rewrite
  - ssl
apache_vhosts:
  - key: bedita
    webroot: /var/www/bedita/bedita-app/webroot
    domain: bedita.example.com
    aliases:
      - be.example.com
      - *.bedita.example.com
      - *.be.example.com
    letsencrypt: yes

letsencrypt_path: "/usr/share/letsencrypt"
letsencrypt_staging: yes

php_extensions:
  - php5-curl
  - php5-gd
  - php5-intl
  - php5-mcrypt
  - php5-mysql
  - php5-redis
  - php5-xcache
  - php5-xdebug
```
